require 'test_helper'

class MailingStatusesControllerTest < ActionController::TestCase
  setup do
    @mailing_status = mailing_statuses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mailing_statuses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mailing_status" do
    assert_difference('MailingStatus.count') do
      post :create, mailing_status: { name: @mailing_status.name }
    end

    assert_redirected_to mailing_status_path(assigns(:mailing_status))
  end

  test "should show mailing_status" do
    get :show, id: @mailing_status
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @mailing_status
    assert_response :success
  end

  test "should update mailing_status" do
    patch :update, id: @mailing_status, mailing_status: { name: @mailing_status.name }
    assert_redirected_to mailing_status_path(assigns(:mailing_status))
  end

  test "should destroy mailing_status" do
    assert_difference('MailingStatus.count', -1) do
      delete :destroy, id: @mailing_status
    end

    assert_redirected_to mailing_statuses_path
  end
end
