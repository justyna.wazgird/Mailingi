require 'test_helper'

class SubjectGroupsControllerTest < ActionController::TestCase
  setup do
    @subject_group = subject_groups(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:subject_groups)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create subject_group" do
    assert_difference('SubjectGroup.count') do
      post :create, subject_group: { name: @subject_group.name }
    end

    assert_redirected_to subject_group_path(assigns(:subject_group))
  end

  test "should show subject_group" do
    get :show, id: @subject_group
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @subject_group
    assert_response :success
  end

  test "should update subject_group" do
    patch :update, id: @subject_group, subject_group: { name: @subject_group.name }
    assert_redirected_to subject_group_path(assigns(:subject_group))
  end

  test "should destroy subject_group" do
    assert_difference('SubjectGroup.count', -1) do
      delete :destroy, id: @subject_group
    end

    assert_redirected_to subject_groups_path
  end
end
