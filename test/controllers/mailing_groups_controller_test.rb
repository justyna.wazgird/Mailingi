require 'test_helper'

class MailingGroupsControllerTest < ActionController::TestCase
  setup do
    @mailing_group = mailing_groups(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mailing_groups)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mailing_group" do
    assert_difference('MailingGroup.count') do
      post :create, mailing_group: { level_group_id: @mailing_group.level_group_id, mailing_id: @mailing_group.mailing_id, other_group_id: @mailing_group.other_group_id, subject_group_id: @mailing_group.subject_group_id }
    end

    assert_redirected_to mailing_group_path(assigns(:mailing_group))
  end

  test "should show mailing_group" do
    get :show, id: @mailing_group
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @mailing_group
    assert_response :success
  end

  test "should update mailing_group" do
    patch :update, id: @mailing_group, mailing_group: { level_group_id: @mailing_group.level_group_id, mailing_id: @mailing_group.mailing_id, other_group_id: @mailing_group.other_group_id, subject_group_id: @mailing_group.subject_group_id }
    assert_redirected_to mailing_group_path(assigns(:mailing_group))
  end

  test "should destroy mailing_group" do
    assert_difference('MailingGroup.count', -1) do
      delete :destroy, id: @mailing_group
    end

    assert_redirected_to mailing_groups_path
  end
end
