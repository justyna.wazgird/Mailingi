require 'test_helper'

class MailingNotesControllerTest < ActionController::TestCase
  setup do
    @mailing_note = mailing_notes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mailing_notes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mailing_note" do
    assert_difference('MailingNote.count') do
      post :create, mailing_note: { name: @mailing_note.name }
    end

    assert_redirected_to mailing_note_path(assigns(:mailing_note))
  end

  test "should show mailing_note" do
    get :show, id: @mailing_note
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @mailing_note
    assert_response :success
  end

  test "should update mailing_note" do
    patch :update, id: @mailing_note, mailing_note: { name: @mailing_note.name }
    assert_redirected_to mailing_note_path(assigns(:mailing_note))
  end

  test "should destroy mailing_note" do
    assert_difference('MailingNote.count', -1) do
      delete :destroy, id: @mailing_note
    end

    assert_redirected_to mailing_notes_path
  end
end
