class CreateMailingNotes < ActiveRecord::Migration
  def change
    create_table :mailing_notes do |t|
      t.string :name

      t.timestamps
    end
  end
end
