class AddIndexes < ActiveRecord::Migration
  def change
    add_index :group_files, :mailing_id
    add_index :mailing_files, :mailing_id
    add_index :mailing_groups, :mailing_id
    add_index :mailing_groups, :level_group_id
    add_index :mailing_groups, :subject_group_id
    add_index :mailing_groups, :other_group_id
    add_index :mailings, :status_id
    add_index :mailings, :assign_to
    add_index :mailings, :author
    add_index :mailings, :send_date
    add_index :mailings, :priority
    add_index :users, :email, unique: true
    add_index :users, :group_id

    drop_table :mailing_notes
    drop_table :tasks
    remove_column :mailings, :note_id

  end
end
