class CreateGroupFiles < ActiveRecord::Migration

	def change

		create_table :group_files do |t|
			t.integer :mailing_id
      t.integer :group_size

      t.attachment :file
			t.timestamps
    end

    create_table :mailing_files do |t|
      t.integer :mailing_id

      t.attachment :file
      t.timestamps
    end

  end



end