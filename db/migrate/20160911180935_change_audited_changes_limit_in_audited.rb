class ChangeAuditedChangesLimitInAudited < ActiveRecord::Migration

  def up
      change_column :audits, :audited_changes, :text, :limit => 16777215
  end

  def down
      change_column :audits, :audited_changes, :text
  end


end