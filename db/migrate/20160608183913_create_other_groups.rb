class CreateOtherGroups < ActiveRecord::Migration
  def change
    create_table :other_groups do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
