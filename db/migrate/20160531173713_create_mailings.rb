class CreateMailings < ActiveRecord::Migration
  def change
    create_table :mailings do |t|
      t.string :name
      t.string :description
      t.date :send_date
      t.date :write_date
      t.date :code_date
      t.date :group_date
      t.integer :status_id
      t.string :preview
      t.integer :assign_to
      t.integer :author
      t.integer :note_id
      t.integer :priority
      t.boolean :has_group

      t.timestamps
    end
  end
end
