class CreateMailingStatuses < ActiveRecord::Migration
  def change
    create_table :mailing_statuses do |t|
      t.string :name

      t.timestamps
	end
  end
end
