class AddDefaultOnMailingHasGroup < ActiveRecord::Migration
	def up
		change_column :mailings, :has_group, :boolean, :default => false
	end

	def down
		change_column :mailings, :has_group, :boolean, :default => nil
	end
end
