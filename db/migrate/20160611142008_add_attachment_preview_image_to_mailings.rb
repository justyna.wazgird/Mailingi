class AddAttachmentPreviewImageToMailings < ActiveRecord::Migration
  def self.up
    change_table :mailings do |t|
      t.attachment :preview_image
    end
  end

  def self.down
    remove_attachment :mailings, :preview_image
  end
end
