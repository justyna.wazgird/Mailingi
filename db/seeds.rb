# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


["zaplanowany", "prawdopodobny", "w trakcie pracy", "do wysyłki", "wysłany"].each do |name|
  MailingStatus.create(:name => name)
end

User.create( :name => 'Justyna Biskup', :email => "jbiskup@gwo.pl", :password => '123456', :group_id => 3 )

["zleceniodawca", "pisarz", "HTMLarz", "DPD"].each do |name|
  UserGroup.create(:name => name)
end

["matematyka", "język polski", "historia", "fizyka", "biologia", "plastyka", "edukacja wczesnoszkolna", "przedszkola"].each do |name|
  SubjectGroup.create(:name => name)
end

["sp", "gim", "sś"].each do |name|
  LevelGroup.create(:name => name)
end

["nasz", "nasz deklarowany", "nasz domniemany", "obcy", "obcy po GWO", "nieokreślony", "nieokreślony po GWO", "Użytkownicy z www (baza GWO)", "Adresy szkolne", "LEPSZA SZKOŁA", "Multimedia"].each do |name|
  OtherGroup.create(:name => name)
end

["Wielkość bazy odbiorców"].each do |name|
  Article.create(:title => name)
end