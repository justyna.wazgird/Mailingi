json.array!(@level_groups) do |level_group|
  json.extract! level_group, :id, :name
  json.url level_group_url(level_group, format: :json)
end
