module SessionsHelper
  
  
  def sign_in(user)
    session[:current_user_id] = user.id
    self.current_user = user
  end

  def signed_in?
    !current_user.nil?
  end
  
  def sign_out
    # Remove the user id from the session
    @current_user = session[:current_user_id] = nil
  end
  

  
  def current_user=(user)
    @current_user = user
  end
  
  
  def current_user
    @current_user ||= session[:current_user_id] && User.find_by_id(session[:current_user_id])
  end

end
