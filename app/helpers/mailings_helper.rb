module MailingsHelper
	def get_priorities
	    Mailing::PRIORITY_TYPES.map { |c| [t("mailing.priority.#{c}"), c] }
	end

	

	def sortable(column, title = nil, params={})
	  title ||= column.titleize
	  css_class = column == sort_column ? "current #{sort_direction}" : nil
	  direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
	  params2 = params.merge({:sort => column, :direction => direction})
	  link_to title, params2, {:class => css_class}
	end


	def calendar_colors(mailing, user)
  		return "#00a65a" if mailing.send?
  		return "#ff851b" if mailing.waiting?  
	  	return "#3c8dbc" if mailing.assigned_to_user(user)  
	  	return "#605ca8"
	end


end
