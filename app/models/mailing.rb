class Mailing < ActiveRecord::Base

	include ActiveModel::Dirty

	audited 
	PRIORITY_TYPES = %w(niski normalny wysoki natychmiastowy)

	has_attached_file :preview_image,
	:styles => { :medium => "300x300>", :thumb => "100x100>" }, 
	:default_url => "/preview_images/:style/missing.png"

	validates_attachment_content_type :preview_image, :content_type => /\Aimage\/.*\Z/

	before_save :set_write_date
	before_save :set_code_date
	before_save :set_blank_to_null
	before_save :set_has_group

	belongs_to :author_id, class_name: "User", foreign_key: "author"
	belongs_to :assign_to_id, class_name: "User", foreign_key: "assign_to"
	belongs_to :mailing_status, foreign_key: "status_id"

	has_many :mailing_groups, :dependent => :destroy
	accepts_nested_attributes_for :mailing_groups, allow_destroy: true, reject_if: :all_blank

	validates :author, presence: true
	# validates :send_date, presence: true
	validates :name, presence: true

	validates_inclusion_of :priority, :in => PRIORITY_TYPES, :allow_blank => true

	has_many :group_files
	has_many :mailing_files

	scope :sentMailings, -> {where(status_id: 5) }
	scope :notsentMailings, -> {where.not(status_id: 5) }

	scope :waitingMailings, -> {where(status_id: 4) }
	scope :inprogressMailings, -> {where(status_id: 3) }
	scope :inplanMailings, -> {where(status_id: 1) }
	scope :indreamsMailings, -> {where(status_id: 2) }

	has_associated_audits

	def self.check_mail
		@group_users = User.where(group_id: "4")
		reminder_date = Date.today + 1
		@group_users.each do |group_user|
		  Mailing.where(send_date: reminder_date).each do |mailing|
		  	if not mailing.has_group_file?
		   		MailingMailer.reminder_group_email(mailing, group_user).deliver_now
		    end
		  end
		end
	end

	def has_group_file?
		self.has_group == true 
	end

	def send?
		self.mailing_status.id == 5 unless self.mailing_status.nil?
	end

	def waiting?
		self.mailing_status.id == 4 unless self.mailing_status.nil?
	end

	def assigned_to_user(user)
		self.assign_to_id.id == user.id unless self.assign_to_id.nil?
	end

	def set_blank_to_null
		self.preview = nil if self.preview == ""
	end

	def set_has_group
		if self.group_changed?
			self.has_group = false 
			self.group_date = nil
		end
		return true
	end

	def group_changed?
		return true if self.other_group_changed?
		self.mailing_groups.each do |mailing_group|
			return true if mailing_group.changed? || mailing_group.new_record? || mailing_group.marked_for_destruction?
		end
		return false
	end

	def set_write_date
		if self.write_date.nil? and assign_to_id != nil then
			if assign_to_id.writer? then
				self.write_date = Date.current
			end
		end
	end

	def set_code_date
		if self.code_date.nil? and assign_to_id != nil then
			if assign_to_id.coder? then
				self.code_date = Date.current
			end
		end
	end


	def self.search(params)
		s = self
  		s = s.where("mailings.id = ?", params['id']) if params['id'].present?
  		s = s.where("name like ?", "%#{params['name']}%") if params['name'].present?
  		s = s.where("description like ?", "%#{params['description']}%") if params['description'].present?
  		s = s.where("status_id = ?", params['status_id']) if params['status_id'].present?
  		s = s.where("author = ?", params['author']) if params['author'].present?
  		s = s.where("assign_to = ?", params['assign_to']) if params['assign_to'].present?
  		s = s.where("priority = ?", params['priority']) if params['priority'].present?
  		s = s.where("has_group = ?", params['has_group'].to_i == 1) if params['has_group'].present?

  		s = s.where("send_date >= ?", params['from']) if params['from'].present?
  		s = s.where("send_date <= ?", params['to']) if params['to'].present?

  		# podstępny hack!!!
  		# - nie robić group, bo popsuje wybieranie liczby wyników
  		# - nie wywalać distinct, bo dodaj join do wyników
  		# - nie optymalizować za pomocą "includes()", bo popsuje się distinct
  		# Rails domyślnie wrzuca do select tylko dane z mailings.*
  		s = s.distinct
  		s = s.joins(:mailing_groups).where("mailing_groups.subject_group_id = ?", params['subject_group_id']) if params['subject_group_id'].present?
  		s = s.joins(:mailing_groups).where("mailing_groups.level_group_id = ?", params['level_group_id']) if params['level_group_id'].present?
  		s = s.joins(:mailing_groups).where("mailing_groups.other_group_id = ?", params['other_group_id']) if params['other_group_id'].present?
  		
  		s
	end


end
