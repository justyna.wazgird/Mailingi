class MailingGroup < ActiveRecord::Base
	belongs_to :subject_group
	belongs_to :level_group
	belongs_to :other_group
	belongs_to :mailing

	audited associated_with: :mailing
end
