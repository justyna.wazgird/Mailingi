class GroupFile < ActiveRecord::Base

	belongs_to :mailing
	audited associated_with: :mailing

	has_attached_file :file,
                    :url => "/:class/:id/:style/:basename.:extension",
                    :path => ":rails_root/private/:class/:attachment/:id_partition/:style/:filename"

	do_not_validate_attachment_file_type :file

	after_save :set_group_size

	after_save :set_mailing_date
	after_destroy :set_no_mailing

	def set_mailing_date
		self.mailing.has_group = true
		self.mailing.group_date = Date.current
		self.mailing.save
	end

	def set_no_mailing
		if self.mailing.group_files.count < 1
			self.mailing.has_group = false
			self.mailing.group_date = nil
			self.mailing.save
		end
	end


	def set_group_size
		if is_csv? or is_txt?
			f = File.open(self.file.path, "r")
			update_column(:group_size, f.readlines.size)
		end
	end


	def is_csv?
		file.instance.file_content_type =~ %r(text/csv)
    end

    def is_txt?
		file.instance.file_content_type =~ %r(text/plain)
    end


end
