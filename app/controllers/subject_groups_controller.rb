class SubjectGroupsController < ApplicationController
  before_action :set_subject_group, only: [:show, :edit, :update, :destroy]

  # GET /subject_groups
  # GET /subject_groups.json
  def index
    @subject_groups = SubjectGroup.all
  end

  # GET /subject_groups/1
  # GET /subject_groups/1.json
  def show
  end

  # GET /subject_groups/new
  def new
    @subject_group = SubjectGroup.new
  end

  # GET /subject_groups/1/edit
  def edit
  end

  # POST /subject_groups
  # POST /subject_groups.json
  def create
    @subject_group = SubjectGroup.new(subject_group_params)

    respond_to do |format|
      if @subject_group.save
        format.html { redirect_to @subject_group, notice: 'Subject group was successfully created.' }
        format.json { render :show, status: :created, location: @subject_group }
      else
        format.html { render :new }
        format.json { render json: @subject_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /subject_groups/1
  # PATCH/PUT /subject_groups/1.json
  def update
    respond_to do |format|
      if @subject_group.update(subject_group_params)
        format.html { redirect_to @subject_group, notice: 'Subject group was successfully updated.' }
        format.json { render :show, status: :ok, location: @subject_group }
      else
        format.html { render :edit }
        format.json { render json: @subject_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /subject_groups/1
  # DELETE /subject_groups/1.json
  def destroy
    @subject_group.destroy
    respond_to do |format|
      format.html { redirect_to subject_groups_url, notice: 'Subject group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subject_group
      @subject_group = SubjectGroup.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def subject_group_params
      params.require(:subject_group).permit(:name)
    end
end
