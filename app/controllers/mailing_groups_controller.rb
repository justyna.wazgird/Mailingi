class MailingGroupsController < ApplicationController
  before_action :set_mailing_group, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @mailing_groups = MailingGroup.all
    respond_with(@mailing_groups)
  end

  def show
    respond_with(@mailing_group)
  end

  def new
    @mailing_group = MailingGroup.new
    respond_with(@mailing_group)
  end

  def edit
  end

  def create
    @mailing_group = MailingGroup.new(mailing_group_params)
    @mailing_group.save
    respond_with(@mailing_group)
  end

  def update
    @mailing_group.update(mailing_group_params)
    respond_with(@mailing_group)
  end

  def destroy
    @mailing_group.destroy
    respond_with(@mailing_group)
  end

  private
    def set_mailing_group
      @mailing_group = MailingGroup.find(params[:id])
    end

    def mailing_group_params
      params.require(:mailing_group).permit(:mailing_id, :level_group_id, :subject_group_id, :other_group_id)
    end
end
