class MailingStatusesController < ApplicationController
  before_action :set_mailing_status, only: [:show, :edit, :update, :destroy]

  # GET /mailing_statuses
  # GET /mailing_statuses.json
  def index
    @mailing_statuses = MailingStatus.all
  end

  # GET /mailing_statuses/1
  # GET /mailing_statuses/1.json
  def show
  end

  # GET /mailing_statuses/new
  def new
    @mailing_status = MailingStatus.new
  end

  # GET /mailing_statuses/1/edit
  def edit
  end

  # POST /mailing_statuses
  # POST /mailing_statuses.json
  def create
    @mailing_status = MailingStatus.new(mailing_status_params)

    respond_to do |format|
      if @mailing_status.save
        format.html { redirect_to @mailing_status, notice: 'Mailing status was successfully created.' }
        format.json { render :show, status: :created, location: @mailing_status }
      else
        format.html { render :new }
        format.json { render json: @mailing_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /mailing_statuses/1
  # PATCH/PUT /mailing_statuses/1.json
  def update
    respond_to do |format|
      if @mailing_status.update(mailing_status_params)
        format.html { redirect_to @mailing_status, notice: 'Mailing status was successfully updated.' }
        format.json { render :show, status: :ok, location: @mailing_status }
      else
        format.html { render :edit }
        format.json { render json: @mailing_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mailing_statuses/1
  # DELETE /mailing_statuses/1.json
  def destroy
    @mailing_status.destroy
    respond_to do |format|
      format.html { redirect_to mailing_statuses_url, notice: 'Mailing status was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mailing_status
      @mailing_status = MailingStatus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def mailing_status_params
      params.require(:mailing_status).permit(:name)
    end
end
