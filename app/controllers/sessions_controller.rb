# encoding: UTF-8

class SessionsController < ApplicationController

  layout false
  
  skip_before_action :require_signin, only: [:new, :create]
  
  # logowanie
  def new
  end

  # zaloguj post
  def create
    user = User.find_by(email: params[:session][:email].downcase)
    
    if user && user.authenticate(params[:session][:password])
      sign_in user
      redirect_to root_path
    else
      flash.now[:alert] = t(:invalid_mail_msg)
      render 'new'
    end
  end


  # wyloguj
  def destroy
    sign_out
    redirect_to root_path
  end
end
