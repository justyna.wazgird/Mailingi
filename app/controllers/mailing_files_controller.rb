class MailingFilesController < ApplicationController
  respond_to :html, :xml, :json
  before_action :set_mailing_file, only: [:show, :edit, :update, :destroy]
  
  def render_file
    mailing_file = MailingFile.find(params[:id])
    send_file mailing_file.file.path(params[:style]), :type => mailing_file.file.content_type
  end

  # GET /mailing_files
  # GET /mailing_files.json
  def index
    @mailing = Mailing.find(params[:mailing_id])
    @mailing_files = @mailing.mailing_files

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @mailing_files }
    end
  end

  # GET /mailing_files/1
  # GET /mailing_files/1.json
  def show
    @mailing_file = MailingFile.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @mailing_file }
    end
  end

  # GET /mailing_files/new
  # GET /mailing_files/new.json
  def new
    @mailing = Mailing.find(params[:mailing_id])
    @mailing_file = @mailing.mailing_files.build

    # respond_modal_with @mailing_file, location: root_path
  end

  # GET /mailing_files/1/edit
  def edit
    @mailing = Mailing.find(params[:mailing_id])
    @mailing_file = MailingFile.find(params[:id])

    # respond_modal_with @mailing_file, location: root_path
  end

  # POST /mailing_files
  # POST /mailing_files.json
  def create
    # @mailing = Mailing.find(params[:mailing_id])

    unless params[:mailing_file][:files].nil?
      params[:mailing_file][:files].each do |document|
        @mailing_file = MailingFile.create(
            file: document,
            mailing_id: params[:mailing_file][:mailing_id],
        )
      end

      redirect_to mailing_path(@mailing_file.mailing, anchor: "mailing_files")
    else
      @mailing_file = @mailing.mailing_files.build
      @mailing_file.errors[:files] = "Wybierz przynajmniej jeden plik"

      # respond_modal_with @mailing_file, location: root_path
    end
  end

  # PUT /mailing_files/1
  # PUT /mailing_files/1.json
  def update
    @mailing_file = MailingFile.find(params[:id])

    unless params[:mailing_file][:files].nil?
      params[:mailing_file][:files].each do |document|
        @mailing_file.file = document
      end
    end

    respond_to do |format|
      if @mailing_file.update(mailing_file_params)
        format.html { redirect_to mailing_path(@mailing_file.mailing, anchor: "mailing_files"), notice: 'MailingFile was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @mailing_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mailing_files/1
  # DELETE /mailing_files/1.json
  def destroy
    @mailing_file = MailingFile.find(params[:id])
    @mailing_file.destroy

    @send_user = @mailing_file.mailing.assign_to_id
    unless @send_user.nil?
      MailingMailer.destroy_file_email(@mailing_file.mailing, @send_user).deliver_now
    end

    respond_to do |format|
      format.html { redirect_to mailing_path(@mailing_file.mailing, anchor: "mailing_files") }
      format.js
    end
  end

  private

    def set_mailing_file
      @mailing_file = MailingFile.find(params[:id])
    end

    def mailing_file_params
      params.require(:mailing_file).permit(:document)
    end
end
