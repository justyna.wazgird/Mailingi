class ApplicationController < ActionController::Base
  layout 'admin_lte_2'

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :require_signin
  before_action :layout_variables, :unless => proc {|c| request.xhr?}
    
  include SessionsHelper

  def layout_variables
    @assign_to_me = Mailing.where(assign_to_id: current_user).notsentMailings.count
    @send = Mailing.sentMailings.count
    @send_today = Mailing.where(send_date: Date.today).notsentMailings.count
    @waiting = Mailing.waitingMailings.count
    @inprogress = Mailing.inprogressMailings.count
    @plan = Mailing.inplanMailings.count
    @dream = Mailing.indreamsMailings.count
  end

  def respond_modal_with(*args, &blk)
    options = args.extract_options!
    options[:responder] = ModalResponder
    respond_with *args, options, &blk
  end
  
  
  private
 
    def require_signin
      unless signed_in?
        flash[:error] = t(:signin_msg)
        redirect_to signin_path # halts request cycle
      end
    end
end
