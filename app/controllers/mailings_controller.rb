class MailingsController < ApplicationController

  respond_to :html
  before_action :set_mailing, only: [:show, :edit, :copy, :update, :destroy, :edit_comment, :save_comment]

  helper_method :sort_column, :sort_direction

  # GET /mailings
  # GET /mailings.json
  def index
    # @mailings = Mailing.all.order("send_date DESC").paginate(:per_page => 10, :page => params[:page])
    @mailings = Mailing.search(params)
 
    respond_to do |format|
      format.html do
        @mailings = @mailings.order(sort_column + " " + sort_direction).paginate(:per_page => 10, :page => params[:page])
      end

      format.xls do
        headers['Content-Type'] = "application/vnd.ms-excel"
        headers['Content-Disposition'] = 'attachment; filename="mailingi - ' + Date.today.to_s + '.xls"'
        headers['Cache-Control'] = ''
      end
    end
  end

  def calendar
    @mailings = Mailing.where.not(send_date: nil).includes(:mailing_status)
  end

  def timeline
    @mailings = Mailing.all.order("created_at DESC").limit(20)
  end

  # GET /mailings/1
  # GET /mailings/1.json
  def show
    @group_size_sum = 0
    @mailing.group_files.each do |group_file|
      @group_size_sum += group_file.group_size unless group_file.group_size.nil?
    end
  end

  def copy
    @old_mailing = Mailing.find(params[:id])

    @new_date = @old_mailing.send_date unless @old_mailing.send_date.nil?
    

    @mailing = Mailing.new(@old_mailing.attributes.merge({ send_date: @new_date, status_id: nil, assign_to_id: nil, id: nil, created_at: nil }))

    @old_mailing.mailing_groups.each do |group|
      @mailing.mailing_groups << group.dup
    end

    respond_to do |format|
      format.html { render action: 'new' }
      format.js
    end

  end

  # GET /mailings/new
  def new
    @mailing = if params[:mailing].nil? then Mailing.new else Mailing.new(mailing_params) end
    @mailing.mailing_groups.build
  end

  # GET /mailings/1/edit
  def edit
    @mailing.mailing_groups.build
  end

  def edit_comment
    respond_modal_with @mailing
  end

  def save_comment

    unless params[:mailing][:mailing_files].nil?
      params[:mailing][:mailing_files][:files].each do |document|
        @mailing.mailing_files.build(file: document)
      end
    end

    assign_to_changed = @mailing.assign_to != mailing_comment_params[:assign_to].to_i
    status_changed = @mailing.status_id != mailing_comment_params[:status_id].to_i

    if @mailing.update(mailing_comment_params)

      @send_user = @mailing.assign_to_id
      unless @send_user.nil?
        if assign_to_changed
          MailingMailer.task_email(@mailing, @send_user).deliver_now
        else
          MailingMailer.change_email(@mailing, @send_user).deliver_now
        end
      end


      @send_author = @mailing.author_id
      if @mailing.send? && status_changed
        MailingMailer.send_email(@mailing, @send_author).deliver_now
      end

      respond_to do |format|
        format.html { redirect_to @mailing, notice: t(:mailing_update_msg) }
        format.js
      end


    else
      respond_to do |format|
        format.html { render action: 'edit_comment' }
        format.js
      end
    end
    
  end

  # POST /mailings
  # POST /mailings.json
  def create
    @mailing = Mailing.new(mailing_params)
    @mailing.author = current_user.id

    respond_to do |format|
      if @mailing.save
        format.html { redirect_to @mailing, notice: 'Mailing został utworzony pomyślnie.' }
        format.json { render :show, status: :created, location: @mailing }

        @send_user = @mailing.assign_to_id
        unless @send_user.nil?
          MailingMailer.task_email(@mailing, @send_user).deliver_now
        end

        # nie wysyłaj automatycznie maili do DPD przy planowaniu maila, bo nie chcą :)
        # @group_users = User.where(group_id: "4")
        # @group_users.each do |group_user|
        #   MailingMailer.group_email(@mailing, group_user).deliver_now
        # end

      else
        format.html { render :new }
        format.json { render json: @mailing.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /mailings/1
  # PATCH/PUT /mailings/1.json
  def update
    assign_to_changed = !mailing_params[:assign_to].blank? && @mailing.assign_to != mailing_params[:assign_to].to_i
    status_changed = @mailing.status_id != mailing_params[:status_id].to_i
    @mailing.attributes = mailing_params
    group_changed = @mailing.group_changed?

    respond_to do |format|
      if @mailing.save
        format.html { redirect_to @mailing, notice: 'Mailing został zaktualizowany.' }
        format.json { render :show, status: :ok, location: @mailing }

        @send_user = @mailing.assign_to_id
        unless @send_user.nil?
          if assign_to_changed
            MailingMailer.task_email(@mailing, @send_user).deliver_now
          else
            MailingMailer.change_email(@mailing, @send_user).deliver_now
          end
        end

        @send_author = @mailing.author_id
        if @mailing.send? && status_changed
          MailingMailer.send_email(@mailing, @send_author).deliver_now
        end

        @group_users = User.where(group_id: "4")
        if group_changed
          @group_users.each do |group_user|
            MailingMailer.change_group_email(@mailing, group_user).deliver_now
          end
        end

      else
        puts @mailing.errors.inspect
        format.html { render :edit }
        format.json { render json: @mailing.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mailings/1
  # DELETE /mailings/1.json
  def destroy
    @mailing.destroy
    respond_to do |format|
      format.html { redirect_to mailings_url, notice: 'Mailing został usunięty.' }
      format.json { head :no_content }
    end
  end

  private
  
    def sort_column
      Mailing.column_names.include?(params[:sort]) ? params[:sort] : "send_date"
    end
    
    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
    end

    def mailing_comment_params 
      params.require(:mailing).permit(:audit_comment, :assign_to, :preview_image, :preview, :status_id,
        mailing_files_attributes: [:id, :document, :_destroy])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_mailing
      @mailing = Mailing.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def mailing_params
      params.require(:mailing).permit(:name, :description, :send_date, :write_date, :code_date, 
        :group_date, :status_id, :preview, :assign_to, :author, :priority, :has_group, 
        :audit_comment, :preview_image, :other_group,
        mailing_files_attributes: [:id, :document, :_destroy],
        mailing_groups_attributes: [:id, :subject_group_id, :level_group_id, :other_group_id, :_destroy])
    end
end
