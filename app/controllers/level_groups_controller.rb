class LevelGroupsController < ApplicationController
  before_action :set_level_group, only: [:show, :edit, :update, :destroy]

  # GET /level_groups
  # GET /level_groups.json
  def index
    @level_groups = LevelGroup.all
  end

  # GET /level_groups/1
  # GET /level_groups/1.json
  def show
  end

  # GET /level_groups/new
  def new
    @level_group = LevelGroup.new
  end

  # GET /level_groups/1/edit
  def edit
  end

  # POST /level_groups
  # POST /level_groups.json
  def create
    @level_group = LevelGroup.new(level_group_params)

    respond_to do |format|
      if @level_group.save
        format.html { redirect_to @level_group, notice: 'Level group was successfully created.' }
        format.json { render :show, status: :created, location: @level_group }
      else
        format.html { render :new }
        format.json { render json: @level_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /level_groups/1
  # PATCH/PUT /level_groups/1.json
  def update
    respond_to do |format|
      if @level_group.update(level_group_params)
        format.html { redirect_to @level_group, notice: 'Level group was successfully updated.' }
        format.json { render :show, status: :ok, location: @level_group }
      else
        format.html { render :edit }
        format.json { render json: @level_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /level_groups/1
  # DELETE /level_groups/1.json
  def destroy
    @level_group.destroy
    respond_to do |format|
      format.html { redirect_to level_groups_url, notice: 'Level group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_level_group
      @level_group = LevelGroup.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def level_group_params
      params.require(:level_group).permit(:name)
    end
end
