class ApplicationMailer < ActionMailer::Base
  require 'digest/sha2'
  default "Message-ID" => ->(v){"<#{Digest::SHA2.hexdigest(Time.now.to_i.to_s)}@gwo.pl>"}
  default from: 'Mailingi GWO <info@gwo.pl>'

  layout 'mailer'
end
