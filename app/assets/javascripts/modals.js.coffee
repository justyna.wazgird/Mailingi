$ ->
  modal_holder_selector = '#modal-holder'
  modal_selector = '.modal'

  $(document).on 'click', 'a[data-modal]', ->
    location = $(this).attr('href')
    #Load modal dialog from server
    $.get location, (data)->
      $(modal_holder_selector).html(data).find(modal_selector).modal()

      # odpalenie całego page:change psuje js po przeładowaniu aktualnie
      $("input.date").datepicker();

      $('.comment').trumbowyg({
        lang: 'pl',
        convertLink: true,
        btns: [
            ['viewHTML'],
            ['formatting'],
            'btnGrp-semantic',
            ['superscript', 'subscript'],
            'btnGrp-justify',
            'btnGrp-lists',
        ]
      });

    false


  $(document).on 'ajax:success', 'form[data-modal]', (event, data, status, xhr)->
    isModal = ! data.match('Turbolinks')

    # js ma content type text/javascript
    # wtedy sie on wykonuje, ale nie umieszczamy go w tresci strony
    # umieszczamy jedynie text/html;
    if isModal && xhr.getResponseHeader("Content-Type").match("text/html;")
      # Remove old modal backdrop
      $('.modal-backdrop').remove()

      # Replace old modal with new one
      $(modal_holder_selector).html(data).
      find(modal_selector).modal()

    false
