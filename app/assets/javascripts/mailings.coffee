# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

duplicateCallback = ($clone, cloner) ->
    $('.destroy_duplicate_mailing_group', $clone).click (e) ->
      cloner.remove(e)


# init dublowania wierszy
$(window).on 'page:change', ->
    duplicateRow = new RowCloner('.duplicatable_mailing_groups', {duplicateCallback: duplicateCallback})
    $('.duplicate_mailing_group').click (e) ->
        duplicateRow.duplicate(e)
    $('.destroy_duplicate_mailing_group').click (e) ->
      duplicateRow.remove(e)



$(document).on 'click', '#mailingSearchResetBtn', ->
	$("#search-form input, #search-form select").val("")
	$("#mailingSearchSubmitBtn").removeAttr('data-disable-with');
	$(this).attr('data-disable-with', $(this).attr('data-disable-with-disable'));
	$("#search-form").submit()