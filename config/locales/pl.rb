# filename: config/locales/pl.rb
# taken from: http://github.com/svenfuchs/i18n/blob/4d8e2e3b8ce9841542cc3cf4d33b7a7702b2abc1/test/test_data/locales/plurals.rb

{:pl => { :i18n => { :plural => { :keys => [:one, :few, :other], :rule => lambda { |n| n == 1 ? :one : [2, 3, 4].include?(n % 10) && ![12, 13, 14].include?(n % 100) && ![22, 23, 24].include?(n % 100) ? :few : :other } } } } }