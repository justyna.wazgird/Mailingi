
email_file = File.join(File.dirname(__FILE__), "../email.yml")

unless File.exist?(email_file)
  raise IOError.new("File email.yml not found. Create email.yml from email.yml.example.")
end

email_config = YAML::load(ERB.new(IO.read(email_file)).result)

if email_config[Rails.env].nil?
  raise LoadError.new("email.yml config for env #{Rails.env} not found")
end

config = email_config[Rails.env].map {|k, v| {k.to_sym => v}}.reduce(:merge)
config[:authentication] = config[:authentication].to_sym unless config[:authentication].blank?

ActionMailer::Base.smtp_settings = config
